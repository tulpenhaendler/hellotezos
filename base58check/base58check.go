package base58check

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"log"
	"math/big"

	"github.com/prettymuchbryce/hellobitcoin/base58check/base58"
)

func Encode(prefix string, byteData []byte) string {

	//Perform SHA-256 twice
	shaHash := sha256.New()
	shaHash.Write(byteData)
	var hash []byte = shaHash.Sum(nil)

	shaHash2 := sha256.New()
	shaHash2.Write(hash)
	hash2 := shaHash2.Sum(nil)

	//First 4 bytes if this double-sha'd byte array is the checksum
	checksum := hash2[0:4]

	//Append this checksum to the input bytes
	encodedChecksum := append(byteData, checksum[0], checksum[1], checksum[2], checksum[3])

	//Convert this checksum'd version to a big Int
	bigIntEncodedChecksum := big.NewInt(10)
	bigIntEncodedChecksum.SetBytes(encodedChecksum)

	//Encode the big int checksum'd version into a Base58Checked string
	base58EncodedChecksum := string(base58.EncodeBig([]byte{}, bigIntEncodedChecksum))


	return base58EncodedChecksum
}

func Decode(value string) []byte {
	zeroBytes := 0

	publicKeyInt, err := base58.DecodeToBig([]byte(value))
	if err != nil {
		log.Fatal(err)
	}

	encodedChecksum := publicKeyInt.Bytes()

	encoded := encodedChecksum[0 : len(encodedChecksum)-4]

	var buffer bytes.Buffer
	for i := 0; i < zeroBytes; i++ {
		zeroByte, err := hex.DecodeString("00")
		if err != nil {
			log.Fatal(err)
		}
		buffer.WriteByte(zeroByte[0])
	}

	buffer.Write(encoded)

	return buffer.Bytes()[1:len(buffer.Bytes())]
}
